FROM amazonlinux:latest

MAINTAINER FAN DENG <fan@bundlelaundry.com>

RUN cp /usr/share/zoneinfo/Australia/Sydney /etc/localtime

RUN yum -y update

## wget
RUN yum install -y wget

## SSHD
RUN yum -y install openssh-server
RUN sed -ri 's/^#PermitRootLogin yes/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    sed -ri 's/^UsePAM yes/UsePAM no/' /etc/ssh/sshd_config

RUN echo 'root:12345678' | chpasswd

## ec2-user
RUN useradd ec2-user

## sudo
RUN yum -y install sudo

EXPOSE 22 80 443

## php7.3
RUN amazon-linux-extras enable php7.3
RUN yum install -y php-cli php-devel php-common php-mbstring php-pdo php-fpm php-json php-mysqlnd php-pdo php-gd php-xml php-mcrypt php-curl php-xsl php-sqlite3 php-json php-zip

## php-xdebug
#RUN yum -y install --enablerepo=remi,remi-php73 --disablerepo=amzn-main php-xdebug
#RUN mv /usr/lib64/php/modules/xdebug.so /usr/lib64/php/7.3/modules

## php.ini
RUN sed -i "s/max_execution_time = 30/max_execution_time = 120/" /etc/php.ini
#COPY php.ini /etc/php-7.3.ini

## composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/tmp
RUN mv /tmp/composer.phar /usr/local/bin/composer

RUN yum install -y curl git tar

# RUN wget https://s3-ap-southeast-2.amazonaws.com/elasticbeanstalk-ap-southeast-2-500038760353/fonts-tlwg-0.6.4.tar.gz && tar xvzf fonts-tlwg-0.6.4.tar.gz -C /usr/share/fonts && rm -f fonts-tlwg-0.6.4.tar.gz && fc-cache /usr/share/fonts -fr

## apache
RUN yum install -y httpd mod_ssl
RUN amazon-linux-extras enable httpd_modules
RUN rm -rf /etc/httpd/conf.d/welcome.conf
RUN cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.bak
RUN sed -ri '/<Directory "\/var\/www\/html">/,/<\/Directory>/s/    AllowOverride None/    AllowOverride All/' /etc/httpd/conf/httpd.conf && \
sed -ri '/<Directory "\/var\/www\/html">/,/<\/Directory>/s/    Options Indexes FollowSymLinks/    Options Indexes FollowSymLinks Includes/' /etc/httpd/conf/httpd.conf && \
sed -ri 's/DirectoryIndex index.html index.html.var/DirectoryIndex index.html index.shtml index.html.var/' /etc/httpd/conf/httpd.conf && \
sed -ri '/DirectoryIndex index.html/s/$/ index.php/g' /etc/httpd/conf/httpd.conf

## make logs dir
RUN mkdir /home/ec2-user/logs

## supervisor
RUN yum install -y python37
RUN curl -O https://bootstrap.pypa.io/get-pip.py
RUN python3 get-pip.py
RUN pip install supervisor
COPY supervisord.conf /etc/supervisor/supervisord.conf

## libs for wkhtmltopdf
RUN yum install -y libXrender fontconfig urw-fonts libXext

COPY etc/conn-site.conf /etc/httpd/conf.d/
COPY etc/conn-site-ssl.conf /etc/httpd/conf.d/

CMD ["/usr/local/bin/supervisord"]